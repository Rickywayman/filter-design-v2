//
//  BandPass.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#include "BandPass.h"

BandPass::BandPass()
{
    setSize (400, 600);
    
    addAndMakeVisible(&BPgainSlider);
    BPgainSlider.setRange(0.0, 1.0, 0.01);
    BPgainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    //BPgainSlider.addListener(this);
    BPgainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //gainSlider.setValue(0.8, true);
    BPgainSlider.setValue(1.0);
    
    addAndMakeVisible(&BPfeedbackGainSlider);
    BPfeedbackGainSlider.setRange(0.0, 1.0, 0.01);
    BPfeedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    //BPfeedbackGainSlider.addListener(this);
    BPfeedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    BPfeedbackGainSlider.setValue(1.0);
    //feedbackGainSlider.setValue(0.5, true);
    
    //Labels
    BPgainLabel.setText("gain", dontSendNotification);
    BPgainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&BPgainLabel);
    
    BPfeedbackLabel.setText("feedback", dontSendNotification);
    BPfeedbackLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&BPfeedbackLabel);
    
}

BandPass::~BandPass()
{
    
}

void BandPass::resized()
{
    int sliderSize = (getWidth()-20)/3;
    if(sliderSize > (getHeight()-60)) sliderSize = getHeight() - 60;
    
    BPgainLabel.setBounds(10, 0, sliderSize, 20);
    BPfeedbackLabel.setBounds(10+sliderSize+sliderSize, 0, sliderSize, 20);
    
    BPgainSlider.setBounds(10, 20, sliderSize, sliderSize);
    BPfeedbackGainSlider.setBounds(10+sliderSize+sliderSize, 20, sliderSize, sliderSize);

}

void BandPass::sliderValueChanged (Slider* slider)
{
    //Lowpass/////////////////////////////////////////////////////////////////////////////////////////////////
    
    if (slider == &BPgainSlider)
    {
        
        BPgain = slider->getValue();
        BPgain = BPgain*BPgain*BPgain; //cubic rule
        
    }
    
    else if (slider == &BPfeedbackGainSlider)
    {
        
        float BPfeedbackGain = slider->getValue();
        BPfeedbackGain = BPfeedbackGain*BPfeedbackGain*BPfeedbackGain;
        //lowpassFilter.setFeedbackGain(BPfeedbackGain);
        //highpassFilter.setFeedbackGain(BPfeedbackGain);
        
    }
    
};