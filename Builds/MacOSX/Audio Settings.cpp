//
//  Audio Settings.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
//==============================================================================
class AudioSettingsDemo  : public Component,
public ChangeListener
{
public:
    AudioSettingsDemo()
    {
        setOpaque (true);
        
        addAndMakeVisible (audioSetupComp
                           = new AudioDeviceSelectorComponent (MainAppWindow::getSharedAudioDeviceManager(),
                                                               0, 256, 0, 256, true, true, true, false));
        
        addAndMakeVisible (diagnosticsBox);
        diagnosticsBox.setMultiLine (true);
        diagnosticsBox.setReturnKeyStartsNewLine (true);
        diagnosticsBox.setReadOnly (true);
        diagnosticsBox.setScrollbarsShown (true);
        diagnosticsBox.setCaretVisible (false);
        diagnosticsBox.setPopupMenuEnabled (true);
        diagnosticsBox.setColour (TextEditor::backgroundColourId, Colour (0x32ffffff));
        diagnosticsBox.setColour (TextEditor::outlineColourId, Colour (0x1c000000));
        diagnosticsBox.setColour (TextEditor::shadowColourId, Colour (0x16000000));
        
        MainAppWindow::getSharedAudioDeviceManager().addChangeListener (this);
        
        logMessage ("Audio device diagnostics:\n");
        dumpDeviceInfo();
    }
    
    ~AudioSettingsDemo()
    {
        MainAppWindow::getSharedAudioDeviceManager().removeChangeListener (this);
    }
    
    void paint (Graphics& g) override
    {
        fillTiledBackground (g);
    }
    
    void resized() override
    {
        Rectangle<int> r (getLocalBounds().reduced (4));
        audioSetupComp->setBounds (r.removeFromTop (proportionOfHeight (0.65f)));
        diagnosticsBox.setBounds (r);
    }
    
    void dumpDeviceInfo()
    {
        AudioDeviceManager& dm = MainAppWindow::getSharedAudioDeviceManager();
        
        logMessage ("--------------------------------------");
        logMessage ("Current audio device type: " + (dm.getCurrentDeviceTypeObject() != nullptr
                                                     ? dm.getCurrentDeviceTypeObject()->getTypeName()
                                                     : "<none>"));
        
        if (AudioIODevice* device = dm.getCurrentAudioDevice())
        {
            logMessage ("Current audio device: " + device->getName().quoted());
            logMessage ("Sample rate: " + String (device->getCurrentSampleRate()) + " Hz");
            logMessage ("Block size: " + String (device->getCurrentBufferSizeSamples()) + " samples");
            logMessage ("Bit depth: " + String (device->getCurrentBitDepth()));
            logMessage ("Input channel names: " + device->getInputChannelNames().joinIntoString (", "));
            logMessage ("Active input channels: " + getListOfActiveBits (device->getActiveInputChannels()));
            logMessage ("Output channel names: " + device->getOutputChannelNames().joinIntoString (", "));
            logMessage ("Active output channels: " + getListOfActiveBits (device->getActiveOutputChannels()));
        }
        else
        {
            logMessage ("No audio device open");
        }
    }
    
    void logMessage (const String& m)
    {
        diagnosticsBox.moveCaretToEnd();
        diagnosticsBox.insertTextAtCaret (m + newLine);
    }
    
private:
    ScopedPointer<AudioDeviceSelectorComponent> audioSetupComp;
    TextEditor diagnosticsBox;
    
    void changeListenerCallback (ChangeBroadcaster*) override
    {
        dumpDeviceInfo();
    }
    
    static String getListOfActiveBits (const BitArray& b)
    {
        StringArray bits;
        
        for (int i = 0; i <= b.getHighestBit(); ++i)
            if (b[i])
                bits.add (String (i));
        
        return bits.joinIntoString (", ");
    }
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioSettingsDemo)
};


// This static object will register this demo type in a global list of demos..
static JuceDemoType<AudioSettingsDemo> demo ("30 Audio: Settings");
