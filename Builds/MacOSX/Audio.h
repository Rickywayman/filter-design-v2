//
//  Audio.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#ifndef __JuceBasicWindow__Audio__
#define __JuceBasicWindow__Audio__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "LowPassFilter.h"
#include "HighpassFilter.h"
#include "ButterworthFilter.h"
#include "DelayLineFilter.h"
#include "ForwardDelayLineFilter.h"

class Audio :   public Component,
                public AudioIODevice,
                public AudioDeviceManager,
                public SliderListener
{
    
public:
    
    Audio();
    
    void closeButtonPressed()
    {
        delete this;
    }
    
    ~Audio();
    
    
    int getfilterType(int type);
    
    
    
    

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Audio)
 

    
    int type;
    float gain;
    float Highgain;
    float BWgain;
    
   
    
    
    
    

    
    
};



#endif /* defined(__JuceBasicWindow__Audio__) */
