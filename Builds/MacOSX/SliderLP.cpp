//
//  SliderLP.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#include "SliderLP.h"

SliderLP::SliderLP()
{
    //setSize (400, 600);
    
    //Lowpass/////////////////////////////////////////////////////////////////////////////////////////////////
    //slider setup
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //gainSlider.setValue(0.8, true);
    gainSlider.setValue(1.0);
    
    addAndMakeVisible(&feedbackGainSlider);
    feedbackGainSlider.setRange(0.0, 1.0, 0.01);
    feedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackGainSlider.addListener(this);
    feedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    feedbackGainSlider.setValue(1.0);
    //feedbackGainSlider.setValue(0.5, true);
    
    //Labels
    gainLabel.setText("gain", dontSendNotification);
    gainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainLabel);
    
    feedbackGainLabel.setText("feedback", dontSendNotification);
    feedbackGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackGainLabel);
}

SliderLP::~SliderLP()
{
    
}

void SliderLP::resized()
{
    
    int sliderSize = (getWidth()-20)/3;
    if(sliderSize > (getHeight()-60)) sliderSize = getHeight() - 60;
    
    gainLabel.setBounds(10, 0, sliderSize, 20);
    feedbackGainLabel.setBounds(10+sliderSize+sliderSize, 0, sliderSize, 20);
    
    gainSlider.setBounds(10, 20, sliderSize, sliderSize);
    feedbackGainSlider.setBounds(10+sliderSize+sliderSize, 20, sliderSize, sliderSize);
}

void SliderLP::sliderValueChanged (Slider* slider)
{
    //Lowpass/////////////////////////////////////////////////////////////////////////////////////////////////
    
    if (slider == &gainSlider)
    {
        
        gain = slider->getValue();
        gain = gain*gain*gain; //cubic rule
        
    }
    else if (slider == &feedbackGainSlider)
    {
        
        float feedbackGain = slider->getValue();
        feedbackGain = feedbackGain*feedbackGain*feedbackGain;
        
        //lowpassFilter.setFeedbackGain(feedbackGain);
        
    }
}

/*
void SliderLP::lowpassGain(LowpassFilter &cLowpassFilter)
{
    
    
}
 */




