//
//  SliderButterworth.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#include "SliderButterworth.h"

SliderButterworth::SliderButterworth()
{
    //setSize (400, 600);
    
    //Butterworth
    //slider setup
    addAndMakeVisible(&BWgainSlider);
    BWgainSlider.setRange(0.0, 1.0, 0.01);
    BWgainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    //BWgainSlider.addListener(this);
    BWgainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    BWgainSlider.setValue(1.0);
    //gainSlider.setValue(0.8, true);
    
    addAndMakeVisible(&BWfeedbackGainSlider);
    BWfeedbackGainSlider.setRange(0.0, 1.0, 0.01);
    BWfeedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    //BWfeedbackGainSlider.addListener(this);
    BWfeedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    BWfeedbackGainSlider.setValue(1.0);
    
    //feedbackGainSlider.setValue(0.5, true);
    
    //Labels
    BWgainLabel.setText("gain", dontSendNotification);
    BWgainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&BWgainLabel);
    
    BWfeedbackGainLabel.setText("feedback", dontSendNotification);
    BWfeedbackGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&BWfeedbackGainLabel);
}

SliderButterworth::~SliderButterworth()
{
    
}

void SliderButterworth::resized()
{
    //Butterworth/////////////////////////////////////////////////////////////////////////////////////////////////
    int BWsliderSize = (getWidth()-20)/3;
    if(BWsliderSize > (getHeight()-60)) BWsliderSize = getHeight() - 60;
    
    BWgainLabel.setBounds(10, 0, BWsliderSize, 20);
    BWfeedbackGainLabel.setBounds(10+BWsliderSize+BWsliderSize, 0, BWsliderSize, 20);
    
    BWgainSlider.setBounds(10, 20, BWsliderSize, BWsliderSize);
    BWfeedbackGainSlider.setBounds(10+BWsliderSize+BWsliderSize, 20, BWsliderSize, BWsliderSize);
}