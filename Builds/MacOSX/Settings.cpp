//
//  Settings.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include "Settings.h"

/*
//==============================================================================
Settings::Settings()
    {
        setOpaque (true);
        
        addAndMakeVisible (audioSetupComp
                           = new AudioDeviceSelectorComponent (MainAppWindow::getSharedAudioDeviceManager(),
                                                               0, 256, 0, 256, true, true, true, false));
        
        
        MainAppWindow::getSharedAudioDeviceManager().addChangeListener (this);
        
        logMessage ("Audio device diagnostics:\n");
        dumpDeviceInfo();
    }
    
    ~AudioSettingsDemo()
    {
        MainAppWindow::getSharedAudioDeviceManager().removeChangeListener (this);
    }
    
    void paint (Graphics& g)
    {
        fillTiledBackground (g);
    }
    
    void resized()
    {
        Rectangle<int> r (getLocalBounds().reduced (4));
        audioSetupComp->setBounds (r.removeFromTop (proportionOfHeight (0.65f)));
        diagnosticsBox.setBounds (r);
    }
    
    void dumpDeviceInfo()
    {
        AudioDeviceManager& dm = MainAppWindow::getSharedAudioDeviceManager();
        
        logMessage ("--------------------------------------");
        logMessage ("Current audio device type: " + (dm.getCurrentDeviceTypeObject() != nullptr
                                                     ? dm.getCurrentDeviceTypeObject()->getTypeName()
                                                     : "<none>"));
        
        if (AudioIODevice* device = dm.getCurrentAudioDevice())
        {
            logMessage ("Current audio device: " + device->getName().quoted());
            logMessage ("Sample rate: " + String (device->getCurrentSampleRate()) + " Hz");
            logMessage ("Block size: " + String (device->getCurrentBufferSizeSamples()) + " samples");
            logMessage ("Bit depth: " + String (device->getCurrentBitDepth()));
            logMessage ("Input channel names: " + device->getInputChannelNames().joinIntoString (", "));
            logMessage ("Active input channels: " + getListOfActiveBits (device->getActiveInputChannels()));
            logMessage ("Output channel names: " + device->getOutputChannelNames().joinIntoString (", "));
            logMessage ("Active output channels: " + getListOfActiveBits (device->getActiveOutputChannels()));
        }
        else
        {
            logMessage ("No audio device open");
        }
    }
    
    void logMessage (const String& m)
    {
        diagnosticsBox.moveCaretToEnd();
        diagnosticsBox.insertTextAtCaret (m + newLine);
    }
    
    void changeListenerCallback (ChangeBroadcaster*) override
    {
        dumpDeviceInfo();
    }
    
    static String getListOfActiveBits (const BitArray& b)
    {
        StringArray bits;
        
        for (int i = 0; i <= b.getHighestBit(); ++i)
            if (b[i])
                bits.add (String (i));
        
        return bits.joinIntoString (", ");
    }
    

};


// This static object will register this demo type in a global list of demos..
static JuceDemoType<AudioSettingsDemo> demo ("30 Audio: Settings");
 
 */
