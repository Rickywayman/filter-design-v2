//
//  BandPass.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#ifndef __JuceBasicWindow__BandPass__
#define __JuceBasicWindow__BandPass__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class BandPass :    public Component,
                    public SliderListener
{
public:
    BandPass();
    ~BandPass();
    
    void resized() override;
    
    void sliderValueChanged	(Slider * 	slider);
    
    
    
private:
    Label BPgainLabel;
    Label BPfeedbackLabel;
    Slider BPgainSlider;
    Slider BPfeedbackGainSlider;
    
    float BPgain;
    
};

#endif /* defined(__JuceBasicWindow__BandPass__) */
