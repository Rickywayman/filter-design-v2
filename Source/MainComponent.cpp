/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
/* MainComponent Constructor - this is where your components are added to the program */
MainComponent::MainComponent()
{
    setSize (400, 600);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback(this); //main component to receive audio callbacks
    
    addAndMakeVisible(&lowpass);
    addAndMakeVisible(&highpass);
    addAndMakeVisible(&butterworth);
    
    //ComboBox
    addAndMakeVisible(filterType);
    filterType.addItem("Lowpass", 1);
    filterType.addItem("Highpass", 2);
    filterType.addItem("Butterworth Lowpass", 3);
    filterType.addItem("Bandpass Filter", 4);
    filterType.setSelectedId(1);
  
    
}
/* */
MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this); //ensure main component stops listening to audio callbacks when closed.
}
/* */
void MainComponent::resized()
{
    int Size = (getWidth());
    if(Size > (getHeight())) Size = getHeight() - 60;
    
    lowpass.setBounds(0, 0, Size, 500);
    highpass.setBounds(0, 200, Size, 500);
    butterworth.setBounds(0, 400, Size, 500);
    
    filterType.setBounds(10, 165, Size - 20, 30);
    
}

int MainComponent::getSelectedId()
{
    int type;

    type = filterType.getSelectedId();
    
    return type;
}

/*
//SliderCallback================================================================
void MainComponent::sliderValueChanged (Slider* slider)
{
    
    
    
    //Hipass/////////////////////////////////////////////////////////////////////////////////////////////////
    
    if (slider == &gainHighSlider)
    {
        
        Highgain = slider->getValue();
        Highgain = Highgain*Highgain*Highgain; //cubic rule
        
    }
   
    else if (slider == &feedbackHighGainSlider)
    {
        
        float feedbackHighGain = slider->getValue();
        feedbackHighGain = feedbackHighGain*feedbackHighGain*feedbackHighGain;
        highpassFilter.setFeedbackGain(feedbackHighGain);
        
    }
    
    //Butterworth/////////////////////////////////////////////////////////////////////////////////////////////////
    
    if (slider == &BWgainSlider)
    {
        
        BWgain = slider->getValue();
        BWgain = BWgain*BWgain*BWgain; //cubic rule
        
    }
  
    else if (slider == &BWfeedbackGainSlider)
    {
        
        float BWfeedbackGain = slider->getValue();
        BWfeedbackGain = BWfeedbackGain*BWfeedbackGain*BWfeedbackGain;
        butterworthFilter.setFeedbackGain(BWfeedbackGain);
        
    }
    
};
 
 */

/* */
void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{
    DBG("audioDeviceAboutToStart");
    float getCurrentSampleRate(AudioIODevice* device);
    //output.addIfNotAlreadyThere(<#const juce::String &stringToAdd#>)
    //audio
    //audioDeviceManager.getCurrentAudioDevice();
   // StringArray getOutputChannelNames(AudioIODevice* device);
}
/* */
void MainComponent::audioDeviceStopped()
{
    DBG("audio device stopped");
}

/*
StringArray MainComponent::getOutputChannelNames()
{
    
    return outputString;
}

void MainComponent::addIfNotAlreadyThere(const String &outputString ,bool ignoreCase = false)
{
    
}
 */

float MainComponent::getLowpassFeedbackGain()
{
    float feedbackgain;
    
    
    
    return feedbackgain;
}


/* Audio Callback - Audio Processing takes place here @ samplerate */
void MainComponent::audioDeviceIOCallback(const float** inputChannelData,
                                  int numInputChannels,
                                  float** outputChannelData,
                                  int numOutputChannels,
                                  int numSamples)

{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    
    while(numSamples--)
    {
        float output = *inL;
        float input = *inL;
        
       
        
        
        if (filterType.getSelectedId() == 1)
        {
            output = lowpassFilter.filter(output) * gain;
        }
        else if(filterType.getSelectedId() == 2)
        {
            output = (highpassFilter.filter(output) * Highgain) + *inL;
        }
        else if(filterType.getSelectedId() == 3)
        {
            output = (butterworthFilter.filter(output) + butterworthFilter.forwardfilter(input)) * BWgain;
        }
        else if(filterType.getSelectedId() == 4)
        {
            output = (lowpassFilter.filter(output) - (highpassFilter.filter(output) + *inL)) * gain;
        }
        
        //Butterworth - im adding the inputs together, find a way around this.
        
        
        *outL = output;
        *outR = output;
        
        
        
        inL++;
        inR++;
        outL++;
        outR++;
        
    }
    
    
};






