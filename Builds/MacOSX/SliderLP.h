//
//  SliderLP.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#ifndef __JuceBasicWindow__SliderLP__
#define __JuceBasicWindow__SliderLP__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "LowPassFilter.h"

class SliderLP :    public Component,
                    public SliderListener
{
public:
    
    SliderLP();
    ~SliderLP();
    
    void resized() override;
    
    void sliderValueChanged	(Slider * 	slider);
    
    //friend void lowpassGain(LowpassFilter &cLowpassFilter);
    
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SliderLP)
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    //LowpassFilter lowpassFilter;
    
    float * FBgain;
    float gain;
    
    
};


#endif /* defined(__JuceBasicWindow__SliderLP__) */
