//
//  ForwardDelayLineFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include "ForwardDelayLineFilter.h"


ForwardDelayLineFilter::ForwardDelayLineFilter()
{
    for (int counter = 0; counter < ASSUMEDSAMPLERATE; counter++)
        forwarddelayLine[counter] = 0.f;
    forwardreadDelaySamples = 1.f;
    forwardwriteIndex = 0;
    forwardfeedbackGain = 0.f;
}
ForwardDelayLineFilter::~ForwardDelayLineFilter()
{
    
}

void ForwardDelayLineFilter::setforwardDelayInSamples(float delayInSamples)
{
    if(delayInSamples >= 0.f && delayInSamples <= ASSUMEDSAMPLERATE-1)
        forwardreadDelaySamples = delayInSamples;
}

void ForwardDelayLineFilter::setforwardFeedbackGain(float val)
{
    if (val >= 0.f && val <= 1.0)
    {
        forwardfeedbackGain = val;
    }
}

float ForwardDelayLineFilter::forwarddelayLineRead()
{
    float readIndex = forwardwriteIndex - forwardreadDelaySamples;
    if (readIndex < 0)
        readIndex += ASSUMEDSAMPLERATE;
    
    int pos1 = static_cast<int>(readIndex);
    int pos2 = pos1 + 1;
    if(pos2 == ASSUMEDSAMPLERATE)
        pos2 = 0;
    
    float fraction = readIndex - static_cast<int>(pos1);
    float amprange = forwarddelayLine[pos2] - forwarddelayLine[pos1];
    float output = forwarddelayLine[pos1] + (fraction * amprange);
    
    return output;
}

void ForwardDelayLineFilter::forwarddelayLineWrite(float input)
{
    if(++forwardwriteIndex == ASSUMEDSAMPLERATE)
        forwardwriteIndex = 0;
    forwarddelayLine[forwardwriteIndex] = input;
}
