//
//  SliderHP.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#ifndef __JuceBasicWindow__SliderHP__
#define __JuceBasicWindow__SliderHP__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class SliderHP :    public Component
//public SliderListener
{
public:
    
    SliderHP();
    ~SliderHP();
    
    void resized() override;
    
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SliderHP)
    
    Slider gainHighSlider;
    Slider feedbackHighGainSlider;
    Label  gainHighLabel;
    Label feedbackHighGainLabel;
    
    
};


#endif /* defined(__JuceBasicWindow__SliderHP__) */
