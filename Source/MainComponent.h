/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "LowPassFilter.h"
#include "HighpassFilter.h"
#include "ButterworthFilter.h"
#include "Audio.h"
#include "AudioSettings.h"
#include "BandPass.h"
#include "SliderLP.h"
#include "SliderHP.h"
#include "SliderButterworth.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public AudioDeviceManager, //inherit from audiodevicemanager
                        public AudioIODeviceCallback //inherit from audioIOdeivcecallback
                        //public SliderListener


{
/* */
public:
    //==============================================================================
    
    /* */
    MainComponent();
    
    
    
    void closeButtonPressed()
    {
        delete this;
    }
    /* */
    ~MainComponent();
    /* */
    void resized() override;
    /* */
    
    /* */
    //int indexOfItemId (int itemId);
    /* */
    int getSelectedId();
    /* */
    //void resized(AudioDeviceSelectorComponent* settings);
    /* */
    //void timerCallback(AudioDeviceSelectorComponent* settings);
    
    //void sliderValueChanged	(Slider * 	slider1);
    
    
    /* override the pure virtual funtions */
    void audioDeviceIOCallback(const float** inputChannelData,
                               int numInputChannels,
                               float** outputChannelData,
                               int numOutputChannels,
                               int numSamples) override;
    
    /* */
    void audioDeviceAboutToStart(AudioIODevice* device) override;
    /* */
    void audioDeviceStopped() override;
    
    //virtual StringArray getOutputChannelNames();
    
    //void addIfNotAlreadyThere();
    
    float getLowpassFeedbackGain();
    
   

/* */
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    /* */
    AudioDeviceManager audioDeviceManager;
    
    //Slider Component
    Slider slider1;
    
    SliderLP lowpass;
    SliderHP highpass;
    SliderButterworth butterworth;
    
    //Lowpass
    Slider gainSlider;
    Slider mixSlider;
    Slider feedbackGainSlider;
    
    //Highpass
    Slider gainHighSlider;
    Slider mixHighSlider;
    Slider feedbackHighGainSlider;
    
    //Butterworth
    Slider BWgainSlider;
    Slider BWmixSlider;
    Slider BWfeedbackGainSlider;
    
    //Unusued (yet to use)
    //DocumentWindow  channel1;
    
    //Labels
    //Lowpass
    Label gainLabel;
    Label mixLabel;
    Label feedbackLabel;
    
    //Highpass
    Label gainHighLabel;
    Label mixHighLabel;
    Label feedbackHighLabel;
    
    //Butterworth
    Label BWgainLabel;
    Label BWmixLabel;
    Label BWfeedbackLabel;
    
    //Private Variables
    float gain;
    float mix;
    float Highgain;
    float Highmix;
    float BWgain;
    float BWmix;
    
    ComboBox filterType;
    
    //Filters
    LowpassFilter lowpassFilter;
    HighpassFilter highpassFilter;
    ButterworthFilter butterworthFilter;
    
    
    
    StringArray outputString;
    StringArray inputString;
    StringArray activeOutputChannelsString;
    StringArray activeInputChannelsString;
    StringArray sampleRateString;
    StringArray audioBufferSizeString;
    
    float *feedbackgain = 0;
    float feedbackvalue;
    
};


#endif  // MAINCOMPONENT_H_INCLUDED
