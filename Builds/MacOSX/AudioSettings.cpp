//
//  AudioSettings.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include "AudioSettings.h"

AudioSettings::AudioSettings()
{
    setSize (400, 400);
    
    addAndMakeVisible(output);
    addAndMakeVisible(input);
    addAndMakeVisible(activeInputChannels); //toggle
    addAndMakeVisible(activeOutputChannels); //toggle
    addAndMakeVisible(sampleRate);
    addAndMakeVisible(audioBufferSize);
    
    addAndMakeVisible(outputLabel);
    addAndMakeVisible(inputLabel);
    addAndMakeVisible(activeInputChannelsLabel);
    addAndMakeVisible(activeOutputChannelsLabel);
    addAndMakeVisible(sampleRateLabel);
    addAndMakeVisible(audioBufferSizeLabel);
    
    outputLabel.setText("Output: ", dontSendNotification );
    inputLabel.setText("Input: ", dontSendNotification );
    activeInputChannelsLabel.setText("Active input channels: ", dontSendNotification );
    activeOutputChannelsLabel.setText("Active output channels: ", dontSendNotification );
    sampleRateLabel.setText("Sample rate: ", dontSendNotification );
    audioBufferSizeLabel.setText("Audio buffer size: ", dontSendNotification );
    
  

    
}

AudioSettings::~AudioSettings()
{
    
}

void AudioSettings::resized()
{
    output.setBounds(170, 30, 200, 25);
    input.setBounds(170, 80, 200, 25);
    activeInputChannels.setBounds(170, 130, 200, 25);
    activeOutputChannels.setBounds(170, 180, 200, 25);
    sampleRate.setBounds(170, 230, 200, 25);
    audioBufferSize.setBounds(170, 280, 200, 25);
    
    outputLabel.setBounds(100, 30, 200, 25);
    inputLabel.setBounds(100, 80, 200, 25);
    activeInputChannelsLabel.setBounds(10, 130, 200, 25);
    activeOutputChannelsLabel.setBounds(10, 180, 200, 25);
    sampleRateLabel.setBounds(70, 230, 200, 25);
    audioBufferSizeLabel.setBounds(30, 280, 200, 25);
    
   
   
  
}


StringArray getOutputChannels()
{
    
}

StringArray getInputChannels()
{
    
}

int getActiveOutputChannels()
{
    
}

int getActiveInputChannels()
{
    
}

void getSampleRate(double[])
{
    
}

void getAudioBufferSize(int[])
{
    
}
