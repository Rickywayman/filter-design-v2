//
//  AudioSettings.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#ifndef __JuceBasicWindow__AudioSettings__
#define __JuceBasicWindow__AudioSettings__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class AudioSettings :   public AudioDeviceManager,
                        public Component
                        
{
public:
    AudioSettings();
    ~AudioSettings();
    
    void resized() override;
    
    StringArray getOutputChannels();
    
    StringArray getInputChannels();
    
    int getActiveOutputChannels();
    
    int getActiveInputChannels();
    
    void getSampleRate(double[]);
    
    void getAudioBufferSize(int[]);
    
    
    
    
    
    
private:
    ComboBox output;
    ComboBox input;
    ComboBox activeOutputChannels;
    ComboBox activeInputChannels;
    ComboBox sampleRate;
    ComboBox audioBufferSize;
    
    Label outputLabel;
    Label inputLabel;
    Label activeOutputChannelsLabel;
    Label activeInputChannelsLabel;
    Label sampleRateLabel;
    Label audioBufferSizeLabel;
    
    
    
  

};


#endif /* defined(__JuceBasicWindow__AudioSettings__) */
